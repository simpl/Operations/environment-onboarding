# Environment Onboarding

To request an environment for any stage please create a new confidential issue here in GitLab by using the provided issue template.
![Issue Template](issue_template.png)\
Make sure to also check the box that this issue is marked as confidential and assign Pascal Pflüger to it.

The environment will be provided in a k8s cluster, including a default stack from the DevSecOps team.\
You, the Dev-Team, can set up everything you need in the k8s cluster via the provided Helm charts.