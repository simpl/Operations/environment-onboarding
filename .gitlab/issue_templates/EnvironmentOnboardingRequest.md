# Environment Onboarding Request

Please fill out this form to request an infrastructure environment for your SIMPL product.

### Environment Name
- xyz

### Stage
- [ ] DEV
- [ ] INT
- [ ] PRE-PROD
- [ ] ACCEPTANCE

### Persons who need access to Rancher / Kubernetes
- Firstname Lastname <mail@example.com>

### List the Namespaces we should create for you
- xyz

### Additional Requirements
- xyz